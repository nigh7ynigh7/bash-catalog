# how to use

places `png`s and `jpg`s in this dir and do the following:

    $ source ./process.sh

then

    $ tessaudience por

it'll take all of the current files and it will try to convert them to text

for other languages do the following (portuguese is assumed as the default language)

    $ tessaudience eng
    $ tessaudience ara
    $ tessaudience jpn
    $ tessaudience ...

other tools will be used to organize data.
