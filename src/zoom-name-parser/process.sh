
tessaudience () {
    #  checks if tesseract is installed or not: tries to install it
    which tesseract || {
        sudo apt update
        sudo apt install -y tesseract-ocr
    }
    which tesseract || {
        echo could not install package
        exit 1
    }

    # temp structures
    mkdir -p .tmp/{imgs,out}

    # dl language pack
    __LANG=$1
    LANG=${__LANG:-"por"}
    cp *.{png,jpg} .tmp/imgs
    curl "https://raw.githubusercontent.com/tesseract-ocr/tessdata_best/master/${LANG}.traineddata" \
        -o .tmp/${LANG}.traineddata

    # process files
    for file in `ls .tmp/imgs`
    do
        tesseract --tessdata-dir .tmp -l $LANG $file .tmp/out/$file
    done

    cat .tmp/out/* > .tmp/out/parsed.txt

    # removes control characters, to lowercase, and some cleanup
    cat .tmp/out/parsed.txt \
        | sed 's/\x0c//g' \
        | sort \
        | uniq \
        | awk '{print tolower($0)}' \
        | awk '{gsub(/(\s|\.|\,)*\(.*/, "")}1;' \
        | sort \
        | uniq > ./final.txt

    # cleanup
    rm -rf .tmp
}
