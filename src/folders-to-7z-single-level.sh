which 7za || {
    echo 'install 7z and 7za with "apt install p7zip-full"'
    exit 1;
};

# how this script works:
#
# it encrypts subfolders and zips them up using 7z.
# the compression is rather modest.

for FOLDER in `ls -d */ | sed 's/\/\+//g'`
do
    # declares current folder
    echo on folder $FOLDER;

    # makes the password using the current date + time + ms
    PASSWORD=`date +%Y%m%d%H%M%S%N | base64`;

    # generate md5 sums
    cd $FOLDER;
    for FILE in `ls`
    do
        md5sum $FILE > $FILE.md5;
    done
    cd ..;

    # makes the password fiole
    echo $PASSWORD > $FOLDER.password_file;
    cp $FOLDER.password_file $FOLDER/$FOLDER.password_file

    # makes the 7z file and encrypts the folder, it also encrypts the file list
    7za a -p$PASSWORD -mx3 -mhe=on -r $FOLDER.7z $FOLDER

    ## this makes a zip file, unlike the previous line, which makes a 7z file
    # zip -r -p -9 $PASSWORD $FOLDER.zip $FOLDER
done


